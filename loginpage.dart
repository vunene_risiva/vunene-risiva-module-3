import 'package:flutter/material.dart';
import 'package:vunenerisiva_module3/dashboard.dart';

class loginpage extends StatelessWidget
{
  const loginpage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context)
  {
    return Scaffold
      (
            appBar:AppBar
              (
                title: Center (child: Text("Login")),
            ),

            body: Column
              (
                mainAxisAlignment:MainAxisAlignment.spaceEvenly,
                crossAxisAlignment: CrossAxisAlignment.center,
                children:[
                  SizedBox(height: 140,width: 140,
                      child: Stack(fit: StackFit.expand,clipBehavior: Clip.none,children: [CircleAvatar(backgroundImage: AssetImage("assets/lady.png"),)],),),
                  TextField(obscureText: true,decoration: InputDecoration(border: OutlineInputBorder(),labelText: 'username')),
                  TextField(obscureText: true,decoration: InputDecoration(border: OutlineInputBorder(),labelText: 'password')),
                  ElevatedButton(onPressed: ()=> {Navigator.push(context,
                      MaterialPageRoute(builder:
                          (context) =>dashboard()))}, child: const Text("Continue")),
                ]
            ),
            bottomNavigationBar:BottomAppBar
              (
                shape: const CircularNotchedRectangle(),
                child: Container(height:30.0)
            )
        );
  }
}
