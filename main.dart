import 'package:flutter/material.dart';
import 'package:vunenerisiva_module3/homepage.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context)
  {
    return MaterialApp
      (
        title: "Flutter",
        theme: ThemeData(

            colorScheme: ColorScheme.fromSwatch(
              primarySwatch: Colors.blueGrey).copyWith(secondary:Colors.black)
            ,
          textTheme:TextTheme(),
          appBarTheme: AppBarTheme(
            backgroundColor: Colors.blueGrey,
            centerTitle: true
          )
        ),
        home: openpage()
      );
  }
}
