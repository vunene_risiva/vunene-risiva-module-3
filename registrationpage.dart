import 'package:flutter/material.dart';
import 'package:vunenerisiva_module3/loginpage.dart';

class registration extends StatelessWidget
{
  const registration({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context)
  {
    String name;
    return Scaffold
          (
            appBar:AppBar
              (
                title: Center (child: Text("Registration Page")),
            ),

            body: Column
              (
                mainAxisAlignment:MainAxisAlignment.spaceEvenly,
                crossAxisAlignment: CrossAxisAlignment.center,
                children:[ Text("Register account"),
                  TextField(obscureText: true,decoration: InputDecoration(border: OutlineInputBorder(),labelText: 'email')),
                  TextField(obscureText: true,decoration: InputDecoration(border: OutlineInputBorder(),labelText: 'username')),
                  TextField(obscureText: true,decoration: InputDecoration(border: OutlineInputBorder(),labelText: 'password')),
                  TextField(obscureText: true,decoration: InputDecoration(border: OutlineInputBorder(),labelText: 'confirm password')),
                  ElevatedButton(onPressed: ()=> {Navigator.push(context,
                      MaterialPageRoute(builder:
                          (context) =>loginpage()))}, child: const Text("Complete")),
            Container(alignment: Alignment.center)
                ]
            ),
            bottomNavigationBar:BottomAppBar
              (
                shape: const CircularNotchedRectangle(),
                child: Container(height:30.0)
            )
        );
  }
}
